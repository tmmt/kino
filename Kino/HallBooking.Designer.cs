﻿namespace Kino
{
    partial class HallBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.receiptButton = new System.Windows.Forms.Button();
            this.movieTitleLabel = new System.Windows.Forms.Label();
            this.screenButton = new System.Windows.Forms.Button();
            this.movieStartTimeLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chosenSeatsNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.takenSeatsNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(85, 143);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(564, 444);
            this.panel1.TabIndex = 0;
            // 
            // receiptButton
            // 
            this.receiptButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.receiptButton.Location = new System.Drawing.Point(523, 602);
            this.receiptButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.receiptButton.Name = "receiptButton";
            this.receiptButton.Size = new System.Drawing.Size(127, 39);
            this.receiptButton.TabIndex = 1;
            this.receiptButton.Text = "Ispiši račun";
            this.receiptButton.UseVisualStyleBackColor = true;
            this.receiptButton.Click += new System.EventHandler(this.receiptButton_Click);
            // 
            // movieTitleLabel
            // 
            this.movieTitleLabel.AutoSize = true;
            this.movieTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.movieTitleLabel.Location = new System.Drawing.Point(83, 32);
            this.movieTitleLabel.Name = "movieTitleLabel";
            this.movieTitleLabel.Size = new System.Drawing.Size(389, 29);
            this.movieTitleLabel.TabIndex = 2;
            this.movieTitleLabel.Text = "Star Wars IV: A New Hope (1977)";
            // 
            // screenButton
            // 
            this.screenButton.Enabled = false;
            this.screenButton.Location = new System.Drawing.Point(85, 114);
            this.screenButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.screenButton.Name = "screenButton";
            this.screenButton.Size = new System.Drawing.Size(533, 23);
            this.screenButton.TabIndex = 3;
            this.screenButton.Text = "Screen";
            this.screenButton.UseVisualStyleBackColor = true;
            // 
            // movieStartTimeLabel
            // 
            this.movieStartTimeLabel.AutoSize = true;
            this.movieStartTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.movieStartTimeLabel.Location = new System.Drawing.Point(84, 71);
            this.movieStartTimeLabel.Name = "movieStartTimeLabel";
            this.movieStartTimeLabel.Size = new System.Drawing.Size(199, 29);
            this.movieStartTimeLabel.TabIndex = 4;
            this.movieStartTimeLabel.Text = "17:40, 28.02.2017";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(81, 612);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Broj odabranih sjedala:";
            // 
            // chosenSeatsNumber
            // 
            this.chosenSeatsNumber.AutoSize = true;
            this.chosenSeatsNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.chosenSeatsNumber.Location = new System.Drawing.Point(277, 612);
            this.chosenSeatsNumber.Name = "chosenSeatsNumber";
            this.chosenSeatsNumber.Size = new System.Drawing.Size(18, 20);
            this.chosenSeatsNumber.TabIndex = 6;
            this.chosenSeatsNumber.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(81, 590);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Broj slobodnih sjedala:";
            // 
            // takenSeatsNumber
            // 
            this.takenSeatsNumber.AutoSize = true;
            this.takenSeatsNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.takenSeatsNumber.Location = new System.Drawing.Point(277, 590);
            this.takenSeatsNumber.Name = "takenSeatsNumber";
            this.takenSeatsNumber.Size = new System.Drawing.Size(18, 20);
            this.takenSeatsNumber.TabIndex = 8;
            this.takenSeatsNumber.Text = "0";
            // 
            // HallBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(755, 690);
            this.Controls.Add(this.takenSeatsNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chosenSeatsNumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.movieStartTimeLabel);
            this.Controls.Add(this.screenButton);
            this.Controls.Add(this.movieTitleLabel);
            this.Controls.Add(this.receiptButton);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HallBooking";
            this.Text = "HallBooking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button receiptButton;
        private System.Windows.Forms.Label movieTitleLabel;
        private System.Windows.Forms.Button screenButton;
        private System.Windows.Forms.Label movieStartTimeLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label chosenSeatsNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label takenSeatsNumber;
    }
}