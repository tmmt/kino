﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class AddMovieForm : Form
    {
        public string name;
        public int year;
        public string genre;
        public int duration;

        public AddMovieForm()
        {
            InitializeComponent();
        }

        private void amf_button1_Click(object sender, EventArgs e)
        {
            this.name = amf_textBox1.Text;
            this.year = (int) amf_numericUpDown1.Value;
            this.genre = amf_textBox2.Text;
            this.duration = (int) amf_numericUpDown2.Value;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
