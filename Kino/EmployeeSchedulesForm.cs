﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class EmployeeSchedulesForm : Form
    {
        private List<EmployeeSchedule> es = new List<EmployeeSchedule>();
        private Employee employee;

        public EmployeeSchedulesForm(Employee employee)
        {
            InitializeComponent();

            this.employee = employee;
            this.Text = employee.first_name + " " + employee.last_name;
            this.es = new EmployeeSchedule().FindEmployeesSchedule(employee.id, DateTime.Today );
            label1.Text = DateTime.Today.ToShortDateString();


            foreach (EmployeeSchedule e in this.es)
            {
                DateTime dt = Convert.ToDateTime(e.start_time);
                dt = dt.AddMinutes(e.duration);
                string finish_time = dt.ToShortTimeString();

                listView1.Items.Add(new ListViewItem(new[] {
                    e.start_time.Remove(0,11).Remove(5),
                    finish_time,
                    e.duration.ToString(),
                    e.job.name
                }));
            }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            DateTime selected = e.Start;
            label1.Text = selected.ToShortDateString();
            List<EmployeeSchedule> esDate = new EmployeeSchedule().FindEmployeesSchedule(employee.id, selected);
            listView1.Items.Clear();
            foreach (EmployeeSchedule a in esDate)
            {
                DateTime dt = Convert.ToDateTime(a.start_time);
                dt= dt.AddMinutes(a.duration);
                string finish_time = dt.ToShortTimeString();

                listView1.Items.Add(new ListViewItem(new[] {
                    a.start_time.Remove(0,11).Remove(5),
                    finish_time,
                    a.duration.ToString(),
                    a.job.name
                }));
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddEmployeeScheduleForm amsf = new AddEmployeeScheduleForm(this.employee);
            amsf.ShowDialog();

            if (amsf.DialogResult != DialogResult.OK)
                return;

            es.Add(amsf.es);

            DateTime dt = Convert.ToDateTime(amsf.es.start_time);
            dt = dt.AddMinutes(amsf.es.duration);
            string finish_time = dt.ToShortTimeString();

            listView1.Items.Add(new ListViewItem(new[] {
                    amsf.es.start_time.Remove(0,11).Remove(5),
                    finish_time,
                    amsf.es.duration.ToString(),
                    amsf.es.job.name
                }));

        }
    }
}
