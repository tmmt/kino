﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class MovieSchedulesForm : Form
    {
        private List<MovieSchedule> ms = new List<MovieSchedule>();
        private Movie movie;

        public MovieSchedulesForm(Movie movie)
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();

            this.movie = movie;
            this.Text = movie.name;

            this.ms = new MovieSchedule().Filter(movie);

            foreach (MovieSchedule m in this.ms)
            {
                listView1.Items.Add(new ListViewItem(new[] {
                    m.start_time.Remove(16),
                    m.Hall.name,
                    m.Hall.hall_type.name,
                    m.seats.Stats()
                }));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddMovieScheduleForm amsf = new AddMovieScheduleForm();
            amsf.ShowDialog();

            if (amsf.DialogResult != DialogResult.OK)
                return;

            MovieSchedule m = new MovieSchedule(movie.id, amsf.hall.id, amsf.start_time, amsf.hall.hall_type.Seats.ToString(), amsf.price);
            m.Save();
            ms.Add(m);
            listView1.Items.Add(new ListViewItem(new[] {
                m.start_time.Remove(16),
                m.Hall.name,
                m.Hall.hall_type.name,
                m.seats.Stats()
            }));
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach (int index in listView1.SelectedIndices)
            {
                MovieSchedule m = this.ms[index];
                HallBooking hb = new HallBooking(m);

                DialogResult dr = hb.ShowDialog();

                if (dr != DialogResult.OK)
                    return;

                this.ms[index] = hb.ms;

                listView1.Items[index] = new ListViewItem(new[] {
                    this.ms[index].start_time.Remove(16),
                    this.ms[index].Hall.name,
                    this.ms[index].Hall.hall_type.name,
                    this.ms[index].seats.Stats()
                });

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
