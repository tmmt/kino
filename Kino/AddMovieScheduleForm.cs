﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class AddMovieScheduleForm : Form
    {
        private List<Hall> hall_choices = new List<Hall>();
        public Hall hall;
        public DateTime start_time;
        public float price;

        public class ComboboxItem
        {
            public string Text { get; set; }
            public Hall Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        public AddMovieScheduleForm()
        {
            InitializeComponent();

            hall_choices = new Hall().All();

            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";

            foreach (Hall h in hall_choices)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = h.name + ": " + h.hall_type.name;
                item.Value = h;

                comboBox1.Items.Add(item);
            }

            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int year = this.monthCalendar1.SelectionRange.Start.Year;
            int month = this.monthCalendar1.SelectionRange.Start.Month;
            int day = this.monthCalendar1.SelectionRange.Start.Day;

            int hour = (int) this.numericUpDown1.Value;
            int minute = (int) this.numericUpDown2.Value;

            this.start_time = new DateTime(year, month, day, hour, minute, 0);
            this.hall = (comboBox1.SelectedItem as ComboboxItem).Value;
            this.price = (float) this.numericUpDown3.Value;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
