﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class HallBooking : Form
    {
        public static Color gray = Color.LightGray;
        public static Color red = Color.Red;
        public static Color yellow = Color.Yellow;
        private String taken = "x";
        private String free = "o";
        private String noChair = "-";
        private String reserve = "r";
        private int w = 20;
        private int h = 20;
        public StringBuilder seatRow = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        private int chairs = 0;
        private int currentChairs = 0;

        public MovieSchedule ms;
        private String layout;
        private String seatStats;
        private string productDescription;
        public DateTime startTime;
        public float productPrice;

        public HallBooking(MovieSchedule ms)
        {
            InitializeComponent();
            this.ms = ms;
            this.layout = ms.seats.ToString();
            this.seatStats = ms.seats.Stats();
            this.productDescription = ms.Movie.name + "(" + ms.Movie.year.ToString() + ")";
            this.startTime = ms.StartTime;
            this.productPrice = ms.price;

            reinitializeSeats();
        }

        private void reinitializeSeats()
        {
            movieTitleLabel.Text = productDescription;
            movieStartTimeLabel.Text = startTime.ToString().Remove(16);
            takenSeatsNumber.Text = seatStats;

            String[] seats = layout.Split(';');
            int seatsInRow =  seats[0].Length;
            screenButton.Size=new Size( seatsInRow * w,screenButton.Height);
            panel1.Size = new Size((seatsInRow +2)* w, panel1.Height);


            int x = 0;
            int y = 0;


            this.panel1.Controls.Clear();

            foreach (String row in seats)
            {
                foreach (Char c in row)
                {
                   if (c.Equals('-'))
                    {
                        this.panel1.Controls.Add(generateLabel(noChair, x, y));
                        x++;
                    }

                    else if (c.Equals('x'))
                    {
                        this.panel1.Controls.Add(generateLabel(taken, x, y));
                        x++;
                    }

                    else if (c.Equals('o'))
                    {
                        this.panel1.Controls.Add(generateLabel(free, x, y));
                        x++;
                    }
                }
                this.panel1.Controls.Add(generateLabel(seatRow[y].ToString(), x, y));
                x = 0;
                y++;
            }
        }

        private Label generateLabel(String availability, int x, int y)
        {
            Label l = new Label();
            l.Name = availability;
            l.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            l.Size = new Size(w, h);
            l.Location = new Point(x * w, y * h);
            if (availability.Equals(noChair))
            {
                l.Visible = false;
            }
            else if (availability.Equals(taken))
            {
                l.BackColor = gray;
            }
            else if (availability.Equals(free))
            {
                l.BackColor = yellow;
                l.Click += new EventHandler(this.keypress);
            }
            else
            {
                l.Text = availability;
                l.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                l.BorderStyle = System.Windows.Forms.BorderStyle.None;
            }
            return l;
        }

        private void keypress(object sender, EventArgs e)
        {
            Label l = (Label)sender;

            if (l.BackColor == yellow)
            {
                l.BackColor = red;
                l.Name = reserve;
                currentChairs++;
                chosenSeatsNumber.Text = currentChairs.ToString();
                
            }
            else if (l.BackColor == red)
            {
                l.BackColor = yellow;
                l.Name = free;
                currentChairs--;
                chosenSeatsNumber.Text = currentChairs.ToString();
            }
        }

        private void receiptButton_Click(object sender, EventArgs e)
        {
            StringBuilder newState = new StringBuilder("");
            foreach (Control la in panel1.Controls)
            {
                if (la is Label)
                {
                    if (la.Name.Equals(noChair)) newState.Append("-");
                    else if (la.Name.Equals(taken)) newState.Append("x");
                    else if (la.Name.Equals(free)) newState.Append("o");
                    else if (la.Name.Equals(reserve))
                    {
                        newState.Append("x");
                        chairs++;
                    }
                    else newState.Append(";");
                }
            }
            newState.Length -= 1;
            layout = newState.ToString();

            //printanje
            PrintDialog printDialog = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDialog.Document = printDocument; //add the document to the dialog box...        
            printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt); // event handler that will do the printing

            DialogResult result = printDialog.ShowDialog(); //možda i ne treba
            if (result == DialogResult.OK)
            {
                printDocument.Print();
                this.ms.seats = new Seats(layout);
                this.ms.Save();
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        public void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float totalprice = 0.00f;
            totalprice = chairs * productPrice;
            float priceWithoutTaxes = totalprice / 1.25f;

            Graphics graphic = e.Graphics;
            Font font = new Font("Courier New", 12);
            Font fontVeci = new Font("Courier New", 12);
            int fontHeight = (int)font.GetHeight() + 5;

            int startX = 10;
            int startCenter = 150;
            int startY = 10;
            int offset = 30;

            //osnovni podaci računa
            graphic.DrawString(" CineBook Movie Place", new Font("Courier New", 18), new SolidBrush(Color.Black), startCenter - 30, startY);
            graphic.DrawString(" Ljuiđi Kolndrekaj", fontVeci, new SolidBrush(Color.Black), startCenter, startY + offset);
            offset += fontHeight;
            graphic.DrawString(" Vile Velebita 1H", fontVeci, new SolidBrush(Color.Black), startCenter, startY + offset);
            offset += fontHeight;
            graphic.DrawString(" OIB: 36257765921", fontVeci, new SolidBrush(Color.Black), startCenter, startY + offset);
            offset += fontHeight;
            graphic.DrawString(" Tel. 0800 400 500", fontVeci, new SolidBrush(Color.Black), startCenter, startY + offset);
            offset += 40;

            //tablica artikala
            string top = "Naziv artikla".PadRight(30) + "Kol.".PadRight(5) + "Cijena".PadRight(10) + "Iznos";
            graphic.DrawString(top, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += fontHeight;
            graphic.DrawString("------------------------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += fontHeight;
            string productLine = productDescription.PadRight(30) + chairs.ToString().PadRight(5) + String.Format("{0:c}", productPrice).PadRight(10) + String.Format("{0:c}", totalprice);
            graphic.DrawString(productLine, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + fontHeight + 15;
            graphic.DrawString("======================================================", font, new SolidBrush(Color.Black), startX, startY + offset);


            //sveukupno
            offset += 20;
            graphic.DrawString("Sveukupno :".PadRight(45) + String.Format("{0:c}", totalprice), new Font("Courier New", 12, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
            offset += 50;

            //porez
            string tax = "Obračun poreza".PadRight(30) + "Osnovica".PadRight(15) + "Iznos";
            graphic.DrawString(tax, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += fontHeight;
            graphic.DrawString("------------------------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += fontHeight;
            string taxLine = "PDV 25.00%:".PadRight(30) + String.Format("{0:c}", priceWithoutTaxes).PadRight(15) + String.Format("{0:c}", totalprice - priceWithoutTaxes);
            graphic.DrawString(taxLine, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += fontHeight;
            graphic.DrawString("------------------------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 20;

            //datum
            DateTime localDate = DateTime.Now;
            var culture = new CultureInfo("hr-HR");
            graphic.DrawString("Zagreb, " + String.Format("{0}", localDate.ToString(culture)), fontVeci, new SolidBrush(Color.Black), startX, startY + offset);
            offset += 40;


            //zahvala
            graphic.DrawString("Hvala na povjerenju, ", font, new SolidBrush(Color.Black), startCenter, startY + offset);
            offset = offset + 15;
            graphic.DrawString(" dođite nam opet!", font, new SolidBrush(Color.Black), startCenter, startY + offset);

        }
    }
}
