﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class AddHallForm : Form
    {
        private List<HallType> hall_type_choices;
        public string name;
        public int hall_type_id;
        public HallType hall_type;

        public class ComboboxItem
        {
            public string Text { get; set; }
            public HallType Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        public AddHallForm()
        {
            InitializeComponent();

            hall_type_choices = new HallType().All();

            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";

            foreach (HallType h in hall_type_choices)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = h.name;
                item.Value = h;

                comboBox1.Items.Add(item);
            }

            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.name = textBox1.Text;
            this.hall_type = (comboBox1.SelectedItem as ComboboxItem).Value;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
