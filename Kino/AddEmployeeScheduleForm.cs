﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class AddEmployeeScheduleForm : Form
    {
        public class ComboboxItem
        {
            public string Text { get; set; }
            public Job Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private List<Job> jobs = new List<Job>();
        public EmployeeSchedule es;
        public Employee employee;

        public AddEmployeeScheduleForm(Employee employee)
        {
            InitializeComponent();

            this.employee = employee;

            jobs = new Job().All();

            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";

            foreach (Job j in jobs)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = j.name;;
                item.Value = j;

                comboBox1.Items.Add(item);
            }

            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int year = this.monthCalendar1.SelectionRange.Start.Year;
            int month = this.monthCalendar1.SelectionRange.Start.Month;
            int day = this.monthCalendar1.SelectionRange.Start.Day;

            int hour = (int)this.numericUpDown1.Value;
            int minute = (int)this.numericUpDown2.Value;
            int duration = (int)this.numericUpDown3.Value;

            Job job = (comboBox1.SelectedItem as ComboboxItem).Value;

            this.es = new EmployeeSchedule(employee.id, job.id, new DateTime(year, month, day, hour, minute, 0), duration);
            this.es.Save();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
