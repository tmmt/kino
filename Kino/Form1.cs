﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class Form1 : Form
    {
        private List<Movie> movies;
        private List<Hall> halls;
        private List<Employee> employees;

        public Form1()
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
            Database db = new Database();

            listView1.View = View.Details;
            listView1.FullRowSelect = true;

            movies = new Movie().All();

            foreach (Movie m in movies)
            {
                listView1.Items.Add(new ListViewItem(new[] {
                    m.name,
                    m.year.ToString(),
                    m.genre,
                    m.duration.ToString()
                }));
            }

            listView2.View = View.Details;
            listView2.FullRowSelect = true;

            halls = new Hall().All();
            foreach (Hall h in halls)
            {
                listView2.Items.Add(new ListViewItem(new[] {
                    h.name,
                    h.hall_type.name,
                    h.hall_type.Seats.SeatNumber().ToString()
                }));
            }

            listView3.View = View.Details;
            listView3.FullRowSelect = true;

            employees = new Employee().All();

            foreach (Employee e in employees)
            {
                listView3.Items.Add(new ListViewItem(new[] {
                    e.first_name,
                    e.last_name,
                    e.oib,
                    e.email,
                    e.telephone
                }));
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            AddMovieForm amf = new AddMovieForm();
            amf.ShowDialog();

            if (amf.DialogResult != DialogResult.OK)
                return;

            Movie m = new Movie(amf.name, amf.year, amf.genre, amf.duration);
            m.Save();
            movies.Add(m);
            listView1.Items.Add(new ListViewItem(new[] {
                m.name,
                m.year.ToString(),
                m.genre,
                m.duration.ToString(),
                m.id.ToString()
            }));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddHallForm ahf = new AddHallForm();
            ahf.ShowDialog();

            if (ahf.DialogResult != DialogResult.OK)
                return;

            Hall h = new Hall(ahf.name, ahf.hall_type);
            h.Save();
            halls.Add(h);
            listView2.Items.Add(new ListViewItem(new[] {
                h.name,
                h.hall_type.name,
                h.hall_type.Seats.SeatNumber().ToString()
            }));
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach (int index in listView1.SelectedIndices)
            {
                Movie movie = movies[index];
                MovieSchedulesForm msf = new MovieSchedulesForm(movie);
                msf.ShowDialog();
            }
        }

        private void listView3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach (int index in listView3.SelectedIndices)
            {
                Employee employee = employees[index];
                EmployeeSchedulesForm esf = new EmployeeSchedulesForm(employee);
                esf.ShowDialog();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddEmployeeForm aef = new AddEmployeeForm();
            aef.ShowDialog();

            if (aef.DialogResult != DialogResult.OK)
                return;

            employees.Add(aef.employee);
            listView3.Items.Add(new ListViewItem(new[] {
                aef.employee.first_name,
                aef.employee.last_name,
                aef.employee.oib,
                aef.employee.email,
                aef.employee.telephone
            }));
        }
    }
}
