﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Kino
{
    class Database
    {
        public Database ()
        {
            if (!System.IO.File.Exists("database.sqlite"))
            {
                SQLiteConnection.CreateFile("database.sqlite");
                CreateTables();
                Fill();
            }
        }

        private void CreateTables()
        {
            new HallType().CreateTable();
            new Hall().CreateTable();
            new Movie().CreateTable();
            new MovieSchedule().CreateTable();
            new Employee().CreateTable();
            new Job().CreateTable();
            new EmployeeSchedule().CreateTable();
        }

        private void DropTables()
        {
            new HallType().DropTable();
            new Hall().DropTable();
            new Movie().DropTable();
            new MovieSchedule().DropTable();
            new Employee().DropTable();
            new Job().DropTable();
            new EmployeeSchedule().DropTable();
        }

        public static string DateTimeSQLite(DateTime datetime)
        {
            string dateTimeFormat = "{0:0000}-{1:00}-{2:00} {3:00}:{4:00}:{5:00}.{6:000}";
            return string.Format(dateTimeFormat, datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
        }

        private void Fill()
        {
            HallType ht1 = new HallType(1, "Extreme", "oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;--------------------;oo----------------oo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo;oooooooooooooooooooo");
            ht1.Save();
            HallType ht2 = new HallType(2, "GoldClass", "oo---oo-oo---oo;---------------;oo---oo-oo---oo;---------------;oo---oo-oo---oo;---------------;oo---oo-oo---oo;---------------;oo---oo-oo---oo;");
            ht2.Save();

            Hall h1 = new Hall(1, "1", ht1);
            h1.Save();
            Hall h2 = new Hall(2, "2", ht1);
            h2.Save();
            Hall h3 = new Hall(3, "3", ht1);
            h3.Save();
            Hall h4 = new Hall(4, "4", ht2);
            h4.Save();
            Hall h5 = new Hall(5, "5", ht2);
            h5.Save();
            Hall h6 = new Hall(6, "6", ht1);
            h6.Save();
            Hall h7 = new Hall(7, "7", ht1);
            h7.Save();
            Hall h8 = new Hall(8, "8", ht2);
            h8.Save();

            Movie m1 = new Movie(1, "Star Wars IV: A New Hope", 1977, "Epic Space Opera", 121);
            m1.Save();
            Movie m2 = new Movie(2, "Star Wars V: The Empire Strikes Back", 1980, "Epic Space Opera", 124);
            m2.Save();
            Movie m3 = new Movie(3, "Star Wars VI: Return of the Jedi", 1983, "Epic Space Opera", 131);
            m3.Save();
            Movie m4 = new Movie(4, "Star Wars VII: The Force Awakens", 2015, "Epic Space Opera", 136);
            m4.Save();

            m1.CreateMovieSchedule(h1, new DateTime(2017, 2, 25, 12, 00, 00), 30).Save();

            m1.CreateMovieSchedule(h1, new DateTime(2017, 2, 28, 12, 00, 00), 30).Save();
            m1.CreateMovieSchedule(h2, new DateTime(2017, 2, 28, 18, 00, 00), 50).Save();
            m1.CreateMovieSchedule(h3, new DateTime(2017, 2, 28, 19, 10, 00), 50).Save();
            m1.CreateMovieSchedule(h4, new DateTime(2017, 4, 28, 20, 30, 00), 150).Save();

            m2.CreateMovieSchedule(h5, new DateTime(2017, 4, 28, 19, 10, 00), 150).Save();
            m2.CreateMovieSchedule(h6, new DateTime(2017, 4, 28, 15, 50, 00), 30).Save();
            m2.CreateMovieSchedule(h7, new DateTime(2017, 4, 28, 18, 00, 00), 40).Save();
            m2.CreateMovieSchedule(h8, new DateTime(2017, 4, 28, 18, 00, 00), 150).Save();

            m3.CreateMovieSchedule(h1, new DateTime(2017, 4, 28, 15, 20, 00), 40).Save();
            m3.CreateMovieSchedule(h2, new DateTime(2017, 4, 28, 21, 00, 00), 50).Save();
            m3.CreateMovieSchedule(h3, new DateTime(2017, 4, 28, 21, 40, 00), 50).Save();
            m3.CreateMovieSchedule(h4, new DateTime(2017, 4, 28, 17, 00, 00), 140).Save();

            m4.CreateMovieSchedule(h5, new DateTime(2017, 4, 28, 22, 20, 00), 150).Save();
            m4.CreateMovieSchedule(h6, new DateTime(2017, 4, 28, 19, 50, 00), 40).Save();
            m4.CreateMovieSchedule(h7, new DateTime(2017, 4, 28, 21, 40, 00), 50).Save();
            m4.CreateMovieSchedule(h8, new DateTime(2017, 4, 28, 21, 00, 00), 150).Save();

            Employee e1 = new Employee(1, "Ivan", "Ivanic", "1029403950392", "ivan.ivanic@gmail.com", "0912345678");
            e1.Save();
            Employee e2 = new Employee(2, "Marija", "Maric", "9502350395935", "marija.maric@gmail.com", "098374759");
            e2.Save();
            Employee e3 = new Employee(3, "Petar", "Peric", "5430964936934", "petar.peric@gmail.com", "0914392851");
            e3.Save();

            Job j1 = new Job(1, "Projekcija filma");
            j1.Save();
            Job j2 = new Job(2, "Blagajna");
            j2.Save();
            Job j3 = new Job(3, "Cuvar");
            j3.Save();

            Job j4 = new Job(4, "Cistac dvorane");
            j4.Save();

            e1.AddJob(j3, new DateTime(2017, 2, 27, 14, 0, 0), 120).Save();
            e1.AddJob(j1, new DateTime(2017, 2, 27, 18, 0, 0), 115).Save();
            e1.AddJob(j4, new DateTime(2017, 2, 27, 20, 20, 0), 15).Save();
            e1.AddJob(j2, new DateTime(2017, 2, 27, 21, 00, 0), 120).Save();


            e1.AddJob(j1, new DateTime(2017, 2, 28, 18, 0, 0), 120).Save();
            e1.AddJob(j2, new DateTime(2017, 2, 28, 20, 0, 0), 180).Save();
            e1.AddJob(j3, new DateTime(2017, 2, 28, 23, 0, 0), 60).Save();

            e2.AddJob(j2, new DateTime(2017, 2, 27, 15, 0, 0), 60).Save();
            e2.AddJob(j3, new DateTime(2017, 2, 27, 16, 0, 0), 100).Save();
            e2.AddJob(j3, new DateTime(2017, 2, 27, 18, 0, 0), 120).Save();

            e2.AddJob(j1, new DateTime(2017, 2, 28, 15, 0, 0), 60).Save();
            e2.AddJob(j2, new DateTime(2017, 2, 28, 16, 0, 0), 120).Save();
            e2.AddJob(j3, new DateTime(2017, 2, 28, 18, 0, 0), 120).Save();

            e3.AddJob(j1, new DateTime(2017, 2, 28, 14, 0, 0), 120).Save();
            e3.AddJob(j2, new DateTime(2017, 2, 28, 18, 0, 0), 120).Save();
            e3.AddJob(j3, new DateTime(2017, 2, 28, 20, 0, 0), 60).Save();
        }
    }

    public class Model
    {
        protected SQLiteConnection Connection;

        public int id = -1;
        public string table;
        private string drop_table = "DROP TABLE {0}";

        public Model(string table)
        {
            Connection = new SQLiteConnection("Data Source=database.sqlite;");

            this.table = table;
        }

        public void delete()
        {
            string delete = String.Format("DELETE FROM {0} WHERE id={1}", this.table, this.id);
            SQLiteCommand command = new SQLiteCommand(delete, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void DropTable()
        {
            string drop = String.Format(this.drop_table, this.table);
            SQLiteCommand command = new SQLiteCommand(drop, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }
    }

    public class HallType: Model
    {
        private string insert = "INSERT INTO '{0}' (name, seats) values ('{1}', '{2}')";
        private string update = "UPDATE '{0}' SET name='{2}', seats='{3}' WHERE id='{1}'";
        private static string create = "CREATE TABLE hall_types (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, seats TEXT)";
        public string name;
        private Seats seats;
        public Seats Seats
        {
            get
            {
                return this.seats;
            }
            set
            {
                this.seats = value;
            }
        }

        public HallType() : base("hall_types") {}

        public HallType(string name, string seats) : base("hall_types")
        {
            this.name = name;
            this.seats = new Seats(seats);
        }

        public HallType(int id, string name, string seats) : this(name, seats)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(HallType.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            HallType h = this.Find(this.id);
            if (this.id < 0 || h == null)
                cmd = String.Format(insert, table, name, seats.ToString());
            else
                cmd = String.Format(update, table, id, name, seats.ToString());

            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public HallType Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new HallType(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"],
                        (string)reader["seats"]
                    );
            }

            Connection.Close();
            return null;
        }

        public List<HallType> All()
        {
            string sql = "select * from " + table;
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<HallType> r = new List<HallType>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new HallType(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"],
                        (string)reader["seats"])
                    );
            }

            Connection.Close();
            return r;
        }
    }


    public class Hall : Model
    {
        private string insert = "INSERT INTO '{0}' (name, hall_type_id) values ('{1}', '{2}')";
        private string update = "UPDATE '{0}' SET name='{2}', hall_type_id='{3}' WHERE id='{1}'";
        private static string create = "CREATE TABLE halls (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, hall_type_id INTEGER)";
        public string name;
        public int hall_type_id;
        public HallType hall_type;

        public Hall() : base("halls") { }

        public Hall(string name, int hall_type_id) : base("halls")
        {
            this.name = name;
            this.hall_type_id = hall_type_id;
            this.hall_type = new HallType().Find(hall_type_id);
        }

        public Hall(int id, string name, int hall_type_id) : this(name, hall_type_id) {
            if (id < 0)
                return;
            this.id = id;
        }

        public Hall(string name, HallType hall_type) : base("halls")
        {
            this.name = name;
            this.hall_type_id = hall_type.id;
            this.hall_type = hall_type;
        }

        public Hall(int id, string name, HallType hall_type) : this(name, hall_type)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(Hall.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            if (id < 0 || this.Find(this.id) == null)
                cmd = String.Format(insert, table, name, hall_type_id);
            else
                cmd = String.Format(update, table, id, name, hall_type_id);

            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public Hall Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new Hall((string)reader["name"], Convert.ToInt32(reader["hall_type_id"]));
            }

            Connection.Close();
            return null;
        }

        public List<Hall> All()
        {
            string sql = "select * from " + table;
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<Hall> r = new List<Hall>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new Hall(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"],
                        Convert.ToInt32(reader["hall_type_id"]))
                    );
            }

            Connection.Close();
            return r;
        }
    }

    public class Movie : Model
    {
        private string insert = "INSERT INTO '{0}' (name, year, genre, duration) values ('{1}', '{2}', '{3}', '{4}')";
        private string update = "UPDATE '{0}' SET name='{2}', year='{3}', genre='{4}', duration='{5}' WHERE id='{1}'";
        private static string create = "CREATE TABLE movies (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, year INT, genre TEXT, duration INTEGER)";
        public string name;
        public int year;
        public string genre;
        public int duration;

        public Movie() : base("movies") { }

        public Movie(string name, int year, string genre, int duration) : base("movies")
        {
            this.name = name;
            this.year = year;
            this.genre = genre;
            this.duration = duration;
        }

        public Movie(int id, string name, int year, string genre, int duration) : this(name, year, genre, duration)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(Movie.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            if (id < 0 || this.Find(this.id) == null)
                cmd = String.Format(insert, table, name, year, genre, duration);
            else
                cmd = String.Format(update, table, id, name, year, genre, duration);
            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public Movie Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new Movie(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"],
                        Convert.ToInt32(reader["year"]),
                        (string)reader["genre"],
                        Convert.ToInt32(reader["duration"])
                    );
            }
            Connection.Close();
            return null;
        }

        public List<Movie> All()
        {
            string sql = "select * from " + table;
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<Movie> r = new List<Movie>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new Movie(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"],
                        Convert.ToInt32(reader["year"]),
                        (string)reader["genre"],
                        Convert.ToInt32(reader["duration"])
                    ));
            }

            Connection.Close();
            return r;
        }

        public MovieSchedule CreateMovieSchedule(Hall hall, DateTime start_time, int price)
        {
            return new MovieSchedule(
                this.id,
                hall.id,
                start_time,
                hall.hall_type.Seats.ToString(),
                price
            );
        }
    }

    public class MovieSchedule : Model
    {
        private string insert = "INSERT INTO '{0}' (movie_id, hall_id, start_time, seats, price) values ('{1}', '{2}', '{3}', '{4}', '{5}')";
        private string update = "UPDATE '{0}' SET movie_id='{2}', hall_id='{3}', start_time='{4}', seats='{5}', price='{6}' WHERE id='{1}'";
        private static string create = "CREATE TABLE movie_schedules (id INTEGER PRIMARY KEY AUTOINCREMENT, movie_id INTEGER, hall_id INTEGER, start_time DATETIME, seats TEXT, price FLOAT)";
        public int movie_id;
        public Movie Movie;
        public int hall_id;
        public Hall Hall;
        public DateTime StartTime;
        public string start_time;
        public Seats seats;
        public float price;

        public MovieSchedule() : base("movie_schedules") { }

        public MovieSchedule(int movie_id, int hall_id, DateTime start_time, string seats, float price) : base("movie_schedules")
        {
            this.movie_id = movie_id;
            this.Movie = new Movie().Find(movie_id);
            this.hall_id = hall_id;
            this.Hall = new Hall().Find(hall_id);
            this.StartTime = start_time;
            this.start_time = Database.DateTimeSQLite(start_time);
            this.seats = new Seats(seats);
            this.price = price;
        }

        public MovieSchedule(int id, int movie_id, int hall_id, DateTime start_time, string seats, float price) : this(movie_id, hall_id, start_time, seats, price)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(MovieSchedule.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            if (id < 0 || this.Find(this.id) == null)
                cmd = String.Format(insert, table, movie_id, hall_id, start_time, seats.ToString(), price);
            else
                cmd = String.Format(update, table, id, movie_id, hall_id, start_time, seats.ToString(), price);
            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public MovieSchedule Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new MovieSchedule(
                        Convert.ToInt32(reader["id"]),
                        Convert.ToInt32(reader["movie_id"]),
                        Convert.ToInt32(reader["hall_id"]),
                        (DateTime)reader["start_time"],
                        (string)reader["seats"],
                        (float) Convert.ToDouble(reader["price"])
                    );
            }

            Connection.Close();
            return null;
        }

        public List<MovieSchedule> All()
        {
            DateTime now = DateTime.Now;
            string sql = "select * from " + table + " WHERE start_time >= '" + Database.DateTimeSQLite(DateTime.Now) + "'";
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<MovieSchedule> r = new List<MovieSchedule>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new MovieSchedule(
                        Convert.ToInt32(reader["id"]),
                        Convert.ToInt32(reader["movie_id"]),
                        Convert.ToInt32(reader["hall_id"]),
                        (DateTime)reader["start_time"],
                        (string) reader["seats"],
                        (float) Convert.ToDouble(reader["price"])
                    ));
            }

            Connection.Close();
            return r;
        }

        public List<MovieSchedule> Filter(Movie movie)
        {
            DateTime now = DateTime.Now;
            string sql = "select * from " + table + " WHERE start_time >= '" + Database.DateTimeSQLite(DateTime.Now) + "' AND movie_id='" + movie.id + "'";
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<MovieSchedule> r = new List<MovieSchedule>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new MovieSchedule(
                        Convert.ToInt32(reader["id"]),
                        Convert.ToInt32(reader["movie_id"]),
                        Convert.ToInt32(reader["hall_id"]),
                        (DateTime)reader["start_time"],
                        (string)reader["seats"],
                        (float) Convert.ToDouble(reader["price"])
                    ));
            }

            Connection.Close();
            return r;
        }
    }


    public class Job : Model
    {
        private string insert = "INSERT INTO '{0}' (name) values ('{1}')";
        private string update = "UPDATE '{0}' SET name='{2}' WHERE id='{1}'";
        private static string create = "CREATE TABLE jobs (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)";
        public string name;

        public Job() : base("jobs") { }

        public Job(string name) : base("jobs")
        {
            this.name = name;
        }

        public Job(int id, string name) : this(name)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(Job.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            if (this.id < 0 || this.Find(this.id) == null)
                cmd = String.Format(insert, table, name);
            else
                cmd = String.Format(update, table, id, name);

            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public Job Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new Job(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"]
                    );
            }

            Connection.Close();
            return null;
        }

        public List<Job> All()
        {
            string sql = "select * from " + table;
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<Job> r = new List<Job>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new Job(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["name"]
                    ));
            }

            Connection.Close();
            return r;
        }
    }

    public class Employee : Model
    {
        private string insert = "INSERT INTO '{0}' (first_name, last_name, oib, email, telephone) values ('{1}', '{2}', '{3}', '{4}', '{5}')";
        private string update = "UPDATE '{0}' SET first_name='{2}', last_name='{3}', oib='{4}', email='{5}', telephone='{6}' WHERE id='{1}'";
        private static string create = "CREATE TABLE employees (id INTEGER PRIMARY KEY AUTOINCREMENT, first_name TEXT, last_name TEXT, oib TEXT, email TEXT, telephone TEXT)";
        public string first_name;
        public string last_name;
        public string oib;
        public string email;
        public string telephone;

        public Employee() : base("employees") { }

        public Employee(string first_name, string last_name, string oib, string email, string telephone) : base("employees")
        {
            this.first_name = first_name;
            this.last_name= last_name;
            this.oib = oib;
            this.email = email;
            this.telephone = telephone;
        }

        public Employee(int id, string first_name, string last_name, string oib, string email, string telephone) : this(first_name, last_name, oib, email, telephone)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(Employee.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            Employee h = this.Find(this.id);
            if (this.id < 0 || h == null)
                cmd = String.Format(insert, table, first_name, last_name, oib, email, telephone);
            else
                cmd = String.Format(update, table, id, first_name, last_name, oib, email, telephone);

            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public Employee Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new Employee(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["first_name"],
                        (string)reader["last_name"],
                        (string)reader["oib"],
                        (string)reader["email"],
                        (string)reader["telephone"]
                    );
            }

            Connection.Close();
            return null;
        }

        public List<Employee> All()
        {
            string sql = "select * from " + table;
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<Employee> r = new List<Employee>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new Employee(
                        Convert.ToInt32(reader["id"]),
                        (string)reader["first_name"],
                        (string)reader["last_name"],
                        (string)reader["oib"],
                        (string)reader["email"],
                        (string)reader["telephone"]
                    ));
            }

            Connection.Close();
            return r;
        }

        public EmployeeSchedule AddJob(Job job, DateTime start_time, int duration)
        {
            return new EmployeeSchedule(this.id, job.id, start_time, duration);
        }
    }


    public class EmployeeSchedule : Model
    {
        private string insert = "INSERT INTO '{0}' (employee_id, job_id, start_time, duration) values ('{1}', '{2}', '{3}', '{4}')";
        private string update = "UPDATE '{0}' SET employee_id='{2}', job_id='{3}', start_time='{4}', duration='{5}' WHERE id='{1}'";
        private static string create = "CREATE TABLE employee_schedules (id INTEGER PRIMARY KEY AUTOINCREMENT, employee_id INTEGER, job_id INTEGER, start_time DATETIME, duration INTEGER)";
        public int employee_id;
        public Employee employee;
        public int job_id;
        public Job job;
        public DateTime StartTime;
        public string start_time;
        public int duration;

        public EmployeeSchedule() : base("employee_schedules") { }

        public EmployeeSchedule(int employee_id, int job_id, DateTime start_time, int duration) : base("employee_schedules")
        {
            this.employee_id = employee_id;
            this.employee = new Employee().Find(employee_id);
            this.job_id = job_id;
            this.job = new Job().Find(job_id);
            this.StartTime = start_time;
            this.start_time = Database.DateTimeSQLite(start_time);
            this.duration = duration;
        }

        public EmployeeSchedule(int id, int employee_id, int job_id, DateTime start_time, int duration) : this(employee_id, job_id, start_time, duration)
        {
            if (id < 0)
                return;
            this.id = id;
        }

        public void CreateTable()
        {
            SQLiteCommand command = new SQLiteCommand(EmployeeSchedule.create, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public void Save()
        {
            string cmd;
            if (id < 0 || this.Find(this.id) == null)
                cmd = String.Format(insert, table, employee_id, job_id, start_time, duration);
            else
                cmd = String.Format(update, table, id, employee_id, job_id, start_time, duration);
            SQLiteCommand command = new SQLiteCommand(cmd, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();

            Connection.Open();
            command.ExecuteNonQuery();
            Connection.Close();
        }

        public EmployeeSchedule Find(int id)
        {
            string s = "select * from " + table + " where id='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                    return new EmployeeSchedule(
                        Convert.ToInt32(reader["id"]),
                        Convert.ToInt32(reader["employee_id"]),
                        Convert.ToInt32(reader["job_id"]),
                        (DateTime)reader["start_time"],
                        Convert.ToInt32(reader["duration"])
                    );
            }

            Connection.Close();
            return null;
        }

        public List<EmployeeSchedule> FindEmployeesSchedule(int id, DateTime date)
        {
            string s = "select * from " + table + " WHERE start_time >= '" + Database.DateTimeSQLite(date) + "%' AND"
                +" start_time <= '" + Database.DateTimeSQLite(date.AddDays(1)) + "%' AND employee_id ='{0}'";
            string sql = String.Format(s, id);

            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<EmployeeSchedule> r = new List<EmployeeSchedule>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new EmployeeSchedule(
                        Convert.ToInt32(reader["id"]),
                        Convert.ToInt32(reader["employee_id"]),
                        Convert.ToInt32(reader["job_id"]),
                        (DateTime)reader["start_time"],
                        Convert.ToInt32(reader["duration"])
                    ));
            }

            Connection.Close();
            return r;
        }

        public List<EmployeeSchedule> All()
        {
            DateTime now = DateTime.Now;
            string sql = "select * from " + table + " WHERE start_time >= '" + Database.DateTimeSQLite(DateTime.Now) + "'";
            SQLiteCommand command = new SQLiteCommand(sql, this.Connection);

            if (Connection.State == System.Data.ConnectionState.Open)
                Connection.Close();
            Connection.Open();

            List<EmployeeSchedule> r = new List<EmployeeSchedule>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                    r.Add(new EmployeeSchedule(
                        Convert.ToInt32(reader["id"]),
                        Convert.ToInt32(reader["employee_id"]),
                        Convert.ToInt32(reader["job_id"]),
                        (DateTime)reader["start_time"],
                        Convert.ToInt32(reader["duration"])
                    ));
            }

            Connection.Close();
            return r;
        }
    }
}
