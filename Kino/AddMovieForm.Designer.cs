﻿namespace Kino
{
    partial class AddMovieForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.amf_label1 = new System.Windows.Forms.Label();
            this.amf_label2 = new System.Windows.Forms.Label();
            this.amf_label3 = new System.Windows.Forms.Label();
            this.amf_label4 = new System.Windows.Forms.Label();
            this.amf_textBox1 = new System.Windows.Forms.TextBox();
            this.amf_numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.amf_numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.amf_textBox2 = new System.Windows.Forms.TextBox();
            this.amf_button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.amf_numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amf_numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // amf_label1
            // 
            this.amf_label1.AutoSize = true;
            this.amf_label1.Location = new System.Drawing.Point(22, 20);
            this.amf_label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.amf_label1.Name = "amf_label1";
            this.amf_label1.Size = new System.Drawing.Size(51, 20);
            this.amf_label1.TabIndex = 0;
            this.amf_label1.Text = "Naziv";
            // 
            // amf_label2
            // 
            this.amf_label2.AutoSize = true;
            this.amf_label2.Location = new System.Drawing.Point(22, 78);
            this.amf_label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.amf_label2.Name = "amf_label2";
            this.amf_label2.Size = new System.Drawing.Size(62, 20);
            this.amf_label2.TabIndex = 1;
            this.amf_label2.Text = "Godina";
            // 
            // amf_label3
            // 
            this.amf_label3.AutoSize = true;
            this.amf_label3.Location = new System.Drawing.Point(22, 133);
            this.amf_label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.amf_label3.Name = "amf_label3";
            this.amf_label3.Size = new System.Drawing.Size(42, 20);
            this.amf_label3.TabIndex = 2;
            this.amf_label3.Text = "Žanr";
            // 
            // amf_label4
            // 
            this.amf_label4.AutoSize = true;
            this.amf_label4.Location = new System.Drawing.Point(15, 182);
            this.amf_label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.amf_label4.Name = "amf_label4";
            this.amf_label4.Size = new System.Drawing.Size(69, 20);
            this.amf_label4.TabIndex = 3;
            this.amf_label4.Text = "Trajanje";
            // 
            // amf_textBox1
            // 
            this.amf_textBox1.Location = new System.Drawing.Point(118, 20);
            this.amf_textBox1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.amf_textBox1.Name = "amf_textBox1";
            this.amf_textBox1.Size = new System.Drawing.Size(657, 26);
            this.amf_textBox1.TabIndex = 4;
            // 
            // amf_numericUpDown1
            // 
            this.amf_numericUpDown1.Location = new System.Drawing.Point(118, 75);
            this.amf_numericUpDown1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.amf_numericUpDown1.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.amf_numericUpDown1.Minimum = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            this.amf_numericUpDown1.Name = "amf_numericUpDown1";
            this.amf_numericUpDown1.Size = new System.Drawing.Size(200, 26);
            this.amf_numericUpDown1.TabIndex = 5;
            this.amf_numericUpDown1.Value = new decimal(new int[] {
            2017,
            0,
            0,
            0});
            // 
            // amf_numericUpDown2
            // 
            this.amf_numericUpDown2.Location = new System.Drawing.Point(118, 178);
            this.amf_numericUpDown2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.amf_numericUpDown2.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.amf_numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.amf_numericUpDown2.Name = "amf_numericUpDown2";
            this.amf_numericUpDown2.Size = new System.Drawing.Size(200, 26);
            this.amf_numericUpDown2.TabIndex = 6;
            this.amf_numericUpDown2.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // amf_textBox2
            // 
            this.amf_textBox2.Location = new System.Drawing.Point(118, 127);
            this.amf_textBox2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.amf_textBox2.Name = "amf_textBox2";
            this.amf_textBox2.Size = new System.Drawing.Size(657, 26);
            this.amf_textBox2.TabIndex = 7;
            // 
            // amf_button1
            // 
            this.amf_button1.Location = new System.Drawing.Point(652, 173);
            this.amf_button1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.amf_button1.Name = "amf_button1";
            this.amf_button1.Size = new System.Drawing.Size(125, 35);
            this.amf_button1.TabIndex = 8;
            this.amf_button1.Text = "DODAJ";
            this.amf_button1.UseVisualStyleBackColor = true;
            this.amf_button1.Click += new System.EventHandler(this.amf_button1_Click);
            // 
            // AddMovieForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 264);
            this.Controls.Add(this.amf_button1);
            this.Controls.Add(this.amf_textBox2);
            this.Controls.Add(this.amf_numericUpDown2);
            this.Controls.Add(this.amf_numericUpDown1);
            this.Controls.Add(this.amf_textBox1);
            this.Controls.Add(this.amf_label4);
            this.Controls.Add(this.amf_label3);
            this.Controls.Add(this.amf_label2);
            this.Controls.Add(this.amf_label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "AddMovieForm";
            this.Text = "Dodaj Film";
            ((System.ComponentModel.ISupportInitialize)(this.amf_numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amf_numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label amf_label1;
        private System.Windows.Forms.Label amf_label2;
        private System.Windows.Forms.Label amf_label3;
        private System.Windows.Forms.Label amf_label4;
        private System.Windows.Forms.TextBox amf_textBox1;
        private System.Windows.Forms.NumericUpDown amf_numericUpDown1;
        private System.Windows.Forms.NumericUpDown amf_numericUpDown2;
        private System.Windows.Forms.TextBox amf_textBox2;
        private System.Windows.Forms.Button amf_button1;
    }
}