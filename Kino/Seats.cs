﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino
{
    public class Seat
    {
        public static char Free = 'o';
        public static char NonExistant = '-';
        public static char Taken = 'x';
        public static char Selected = 'r';
        public static char Separator = ';';

        private char type = '?';
        public string Type
        {
            get
            {
                return this.type.ToString();
            }
        }

        public Seat(char type)
        {
            if (type == Separator)
                return;
            if (type != Free && type != NonExistant && type != Taken && type != Selected)
                type = Free;
            this.type = type;
        }

        public Seat(string type) : this(Convert.ToChar(type)) { }

        public bool Select()
        {
            if (this.type != Free)
                return false;

            this.type = Selected;
            return true;
        }

        public bool Deselect()
        {
            if (this.type != Selected)
                return false;

            this.type = Free;
            return true;
        }

        public override string ToString()
        {
            return this.type.ToString();
        }

        public Seat Confirm()
        {
            if (this.type == Selected)
                this.type = Taken;
            return this;
        }

        public Boolean IsNonExistant()
        {
            return this.type == NonExistant;
        }

        public int Count()
        {
            return Convert.ToInt32(this.type == Free);
        }

        public int SeatNumber()
        {
            return Convert.ToInt32(this.type != NonExistant);
        }
    }

    public class Row : System.Collections.IEnumerable
    {
        private List<Seat> row = new List<Seat>();
        public Row(string row)
        {
            foreach (char c in row)
                this.row.Add(new Kino.Seat(c.ToString()));
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            foreach (Seat s in this.row)
                yield return s;
        }

        public Seat this[int number]
        {
            get
            {
                return this.row[number];
            }
            set
            {
                this.row[number] = value;
            }
        }

        public override string ToString()
        {
            string ret = "";
            foreach (Seat s in this)
                ret += s.ToString();
            return ret;
        }

        public Row Confirm()
        {
            foreach (Seat s in this)
                s.Confirm();
            return this;
        }

        public Boolean IsNonExistant()
        {
            foreach (Seat s in this)
                if (!s.IsNonExistant())
                    return false;
            return true;
        }

        public int Count()
        {
            int count = 0;
            foreach (Seat s in this)
                count += s.Count();
            return count;
        }

        public int SeatNumber()
        {
            int count = 0;
            foreach (Seat s in this)
                count += s.SeatNumber();
            return count;
        }
    }

    public class Seats : System.Collections.IEnumerable
    {
        protected List<Row> rows = new List<Row>();

        public Seats() { }

        public Seats(string seats)
        {
            foreach (string row in seats.Split(Seat.Separator))
                if (row != "")
                    this.rows.Add(new Kino.Row(row));
        }

        override public string ToString()
        {
            string ret = "";
            foreach (Row row in this.rows)
                ret += row.ToString() + Seat.Separator.ToString();

            return ret.Remove(ret.Length - 1);
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            foreach (Row row in this.rows)
                yield return row;
        }

        public Row this[int number]
        {
            get
            {
                return this.rows[number];
            }
            set
            {
                this.rows[number] = value;
            }
        }

        public Seats Confirm()
        {
            foreach (Row row in this)
                row.Confirm();
            return this;
        }

        public int Count()
        {
            int count = 0;
            foreach (Row r in this)
                count += r.Count();
            return count;
        }

        public int SeatNumber()
        {
            int count = 0;
            foreach (Row r in this)
                count += r.SeatNumber();
            return count;
        }

        public string Stats()
        {
            return String.Format("{0}/{1}", this.Count(), this.SeatNumber());
        }
    }
}
