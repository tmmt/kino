﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino
{
    public partial class AddEmployeeForm : Form
    {
        public string first_name;
        public string last_name;
        public string oib;
        public string email;
        public string telephone;
        public Employee employee;

        public AddEmployeeForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.first_name = textBox1.Text;
            this.last_name = textBox2.Text;
            this.oib = textBox3.Text;
            this.email = textBox4.Text;
            this.telephone = textBox5.Text;

            this.employee = new Employee(first_name, last_name, oib, email, telephone);
            this.employee.Save();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
